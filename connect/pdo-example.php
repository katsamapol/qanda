<?php
require_once("/connect.php");
ini_set('display_errors', 'On');
error_reporting(E_ALL);
//Connecting. DSN
$host   = 'localhost';
$db     = 'redemption';
$user   = 'root';
$pass   = 'jai';
/*
FETCH_MODE
PDO::FETCH_NUM returns enumerated array
PDO::FETCH_ASSOC returns associative array
PDO::FETCH_BOTH - both of the above (Default)
PDO::FETCH_OBJ returns object
PDO::FETCH_LAZY allows all three (numeric associative and object) methods without memory overhead.
*/
/*
Error handling. Exceptions
Although there are several error handling modes in PDO, the only proper one is PDO::ERRMODE_EXCEPTION. So, one ought to always set it this way, either by adding this line after creation of PDO instance,
*/
$dsn = "mysql:host=".$host.";dbname=".$db;
$opt = array(
    PDO::ATTR_ERRMODE           => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE=> PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES  => false,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );

try {
    $pdo = new PDO($dsn, $user, $pass, $opt);
} catch (PDOException $e) {
    die('Connection failed: '.$e->getMessage());
}

//Running queries. PDO::query()
try {
    $pdo_query = $pdo->query("SELECT `username` FROM user");
} catch (PDOException $e) {
    die('Query failed: '.$e->getMessage());
}
while ($row = $pdo_query->fetch())
{
    echo $row['username'] . "<br/>";
}

//Prepared statements. Protection from SQL injections (Hacker)
//prepare query and data;
$identification = "3102002902007";
$phone = "0866337662";
$fName="เอมอร";
$sName="ศรีสมยง";
$searchArray = array($identification,$phone);
//$sql="SELECT `id` FROM `funds` WHERE `fname` = ? AND `sname` = ?";
$sql="SELECT `id`,`fname`,`sname`,`phone` FROM `funds` WHERE `id` = ? AND `phone` = ?";
//execute
$pdoPrepare = $pdo->prepare($sql);
try{
    $pdoPrepare->execute($searchArray);
}catch(PDOException $e){
    die ('Execute failed: '. $e->getMessage());
}

$result = $pdoPrepare->fetch();
//print_r($pdoPrepare);
print_r($result);
echo "<br/>";
$sql="SELECT `id`,`fname`,`sname`,`phone` FROM `funds` WHERE `id` = :id AND `phone` = :phone";
$searchArray = array(':id'=>$identification,':phone'=>$phone);
$pdoPrepare = $pdo->prepare($sql);
try{
    $pdoPrepare->execute($searchArray);
}catch(PDOException $e){
    die ('Execute failed: '. $e->getMessage());
}
$result = $pdoPrepare->fetch();
//print_r($pdoPrepare);
print_r($result);
echo "<br/>";

$sql="SELECT `id`,`fname`,`sname`,`phone` FROM `funds` WHERE `fname` = :fName";
$searchArray = array(':fName'=>$fName);
$pdoPrepare = $pdo->prepare($sql);
try{
    $pdoPrepare->execute($searchArray);
}catch(PDOException $e){
    die ('Execute failed: '. $e->getMessage());
}
//$pdoPrepare->debugDumpParams();
//break;
$result = $pdoPrepare->fetch();
//print_r($pdoPrepare);
print_r($result);
echo "<br />";

//Binding methods
$sql="SELECT `id`,`fname`,`sname`,`phone`,`eq` FROM `funds` WHERE `id` = :id AND`fname` = :fName AND `sname` = :sName";
$pdoPrepare = $pdo->prepare($sql);
//http://th1.php.net/manual/en/pdo.constants.php
$pdoPrepare->bindParam(':id',$identification, PDO::PARAM_INT); //if you change $identification this will change too.
$pdoPrepare->bindParam(':sName',$sName, PDO::PARAM_STR);
$pdoPrepare->bindValue(':fName',$fName); //if you change $fName this will not change
try{
    $pdoPrepare->execute();
}catch(PDOException $e){
    die ('Execute failed: '. $e->getMessage());
}
//$pdoPrepare->debugDumpParams();
//break;
$result = $pdoPrepare->fetch();
//print_r($pdoPrepare);
print_r($result);

echo "<br />";
//call html entities to prevent html from being entered
$msg = htmlentities($msg, ENT_QUOTES);

$sql = "UPDATE `funds` SET `eq` = :eq WHERE `id` = :id";
$eq="0";
$pdoPrepare2 = $pdo->prepare($sql);
$pdoPrepare2->bindParam(':eq',$eq, PDO::PARAM_INT);;
$pdoPrepare2->bindParam(':id',$identification, PDO::PARAM_INT);
try{
    $pdoPrepare2->execute();
    echo "Executed : ".$pdoPrepare2->rowCount()."<br/>";
}catch(PDOException $e){
    die('Execute failed: '. $e->getMessage());
}
$result = $pdoPrepare->fetch();
print_r($result);


//Transactions
try{
    $pdo->beginTransaction();
    //do something
    $pdoPrepare3 = $pdo->prepare("INSERT INTO user (username) VALUES (:username)");
    foreach(array("usernamex","usernamey") as $username){
        $pdoPrepare3->execute(array(":username"=>$username));
    }
    $pdo->commit();
    //if OK commmit it
}catch (Exception $e){
    $pdo->rollback();
    throw $e;
}
?>