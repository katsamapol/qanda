//http://api.jquery.com/
//TEST jQuery
// <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
if(jQuery){
    alert("jQuery loaded!");
}else{
    alert("No jQuery");
}
//clear console
clear();
//Selector
$("*");//select all
$("div");//select div
$("h1");//select h1
$("#identifier");//select #identifier
$(".class");//select class
$("div.class");//select .class inside div
$("div#identifier");//select #identifier inside div
$("div h1");//select h1 after div
$("div:first")//select first of div (slower)
$("div:first-of-type")//select first div (native faster)
$("div:last-of-type")//select last div
//Assign css - backgroundColor, width, color, fontSize, border
//1
$("foo").css("color","yellow");
//2
$("foo").css({
    color: "yellow",
    backgroundColor: "rgba(89, 45, 20, 0.5)",
    fontSize: "18px",
    border: "1px solid black"
});
//3
var styles = {
    color: "yellow",
    backgroundColor: "rgba(89, 45, 20, 0.5)",
    fontSize: "18px",
    border: "1px solid black"
}
$("foo").css(styles);
//Assign text
$("foo").text();//return current selector's text
$("foo").text("<h1><a href='http://www.google.com'>some text</a></h1>");//<h1><a href='http://www.google.com'>some text</a></h1>
//Assign HTML content
$("foo").html();//return current selector's html
$("foo").html("<h1><a href='http://www.google.com'>some text</a></h1>");//some text with link to google.com
//Assign attribute value e.g. change image picture, change title, change type
//<img id="greatphoto" src="brush-seller.jpg" alt="brush seller">
$("foo").attr("alt");//brush seller
$("foo").attr("alt","brush buyer")//brush buyer
$("foo").attr({
    src: "brush-buyer.jpg",
    alt: "brush buyer"
});
//Assign value of selected element
$("select[name=foo] option:selected").val();//get selecting option of select box
$("select[name=foo]").val();//easier way to get selecting option of select box
$("input:checkbox[name:foo]:checked").val();//get the value from a checked checkbox
$("input[type=checkbox][name:foo]:checked").val();//work better
$("input:radio[name=foo]:checked").val();//get the value from a set radio buttons
$("input[type=radio][name=foo]:checked").val();//work better
$("select#identifier").val();//get selecting option of select box
$("select.class").val();//get selecting option of select box
$("input[type=text]").val("Katsamapol");//Assign text name foo value to Katsamapol

//Add Remove Change Class of element
$("h1").addClass("className1")//Assign className1 to h1
$("h1").addClass("className2")//Assign className2 to h1 now h1 has both className1 and className2
$("h1").removeClass("className1")//Remove className1 to h1 now h1 has only className2
$("h1").toggleClass("className1")//Toggle className1 to h1 if present remove it else assign it now h1 has both className1 and className2

//Event Handler
// click(),keypress(),on() rule them all.
$("#submit").click(function(){
    console.log("Another click");
});//If someone click id=submit show log "Another click" in console.
$("button").click(function(){
    alert("Someone clicked a button");
});//If someone click any button show alert "Someone clicked a button".
$("button").click(function(){
    $(this).css("backgroundColor","purple");
});//If someone click any button changed its background color to purple
$("button").click(function(){
    var text = $(this).text();
    console.log("Button text = " + text);
});//if someone click any button show log Button text = its text.

//Note that keydown and keyup provide a code indicating which key is pressed,
//While keypress indicates which character was entered.
//For example a lowercase "a" will be reported as 65 by keydown and keyup, but as 97 by keypress.
//An uppercase "A" is reported as 65 by all events.
//Because of this distinction, when catching special keystrokes such as arrow keys, .keydown() or .keyup() is a better choice.
$("input[type=text]").keypress(function(){
    console.log("Handler for .keypress() called.");
});
$("button").click(function(){
    $("#target").keypress();
});//press #target key eg: a b c d e f use to build keyboard function
$("input").keypress(function(e){
    var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
    if (charCode) {
        console.log("Character typed: " + String.fromCharCode(charCode) + " Key #"+charCode);
    }
});//Check which key user pressed and log it to console.
$("#submit").on("click", function(){
    console.log("Another click");
});//If someone click id=submit log "Another click" to console.
//http://api.jquery.com/on
// Why use on click instead on click http://jsfiddle.net/AJRw3/
// click event doesn't listen to new element after page loaded but on click does.

//jQuery Effects
//http://api.jquery.com/?s=effect
//fadeOut(), fadeIn(), fadeTo(), animate(), clearQueue(), delay(), slide() etc.
$("button").on("click", function(){
    $("div").fadeOut(1000, function(){//fade all div out in 1 second.
        $(this).remove();//remove all div after fade them out.
        console.log("Fade Completed");
    });
});//If someone click a button fade all div out in 1 second and then log "Fade Complete" to console.

$("button").on("click", function(){
    $("div").fadeToggle();//Toggle fade in and out.
});
$("button").on("click", function(){
    $("div").slideToggle();//Side up and down
});
