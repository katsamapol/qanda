// if(jQuery){
//     alert("jQuery loaded!");
// }else{
//     alert("No jQuery");
// }

$(document).ready(function(){
    $("#alertMessage").fadeOut(0);
    $("#newMessage").fadeOut(0);
    $("#chatBackButton").on("click", function(){
        window.location.replace("list.php");
        return false;
    });
    getBadWordList();
    var chatWindow = $("#chatWindow"),
        scrollTo = $(".message:last");
    chatWindow.scrollTop(chatWindow.prop('scrollHeight'));
    var alertCount = 0;
    var isDisabled = false;
    var disabledTimeOut;
    var targetTimeOut;
    var alertMessageTimeOut;
    var newMessageTimeOut;
    var timeOut = 30;
    $("#newMessage").on("click", function(){
        $("#newMessage").fadeOut(0);
        chatWindow.scrollTop(chatWindow.prop('scrollHeight'));
    });
    $("#chatSendButton").on("click", function(){
        var msg = $("#chatTextInput").val();
        if(msg.trim().length > 0){
            if(isDisabled === false){
            // $("#chatSendButton").prop("disabled",true);
                isDisabled = true;
                postMsg();
                disabledTimeOut = setTimeout(function(){
                    isDisabled = false;
                }, 1000*timeOut);
                 targetTimeOut = new Date().getTime() + (1000*timeOut);
            }else{
                //console.log(disabledTimeOut);
                var countDown = Math.floor((targetTimeOut - new Date().getTime())/1000);
                // var countDown = targetTimeOut - alertTimeOut;
                $("#alertMessage").text("กรุณารอ "+countDown+" วินาทีก่อนส่งข้อความถัดไป");
                $("#alertMessage").fadeIn(100,function(){
                    if(alertMessageTimeOut){
                        clearTimeout(alertMessageTimeOut);
                    }
                    alertMessageTimeOut = setTimeout(function(){
                        $("#alertMessage").fadeOut(100);
                    }, 3000);
                });
            }
        }
    });
    //check for new message
    newMessageTimeOut = setInterval(function(){
        //$("#alertMessage").fadeOut(100);
        //console.log("lastChatLogId: "+$("#lastChatLogId").val());
        //console.log("idTopic: "+$("#idTopic").val());
        //console.log("guestName: "+$("#guestName").val());
        getChat($("#lastChatLogId").val(),$("#idTopic").val(),$("#guestName").val());
        // chatWindow.scrollTop(
        //     scrollTo.offset().top - chatWindow.offset().top + chatWindow.scrollTop()
        // );
    //     chatWindow.scrollTop(chatWindow.prop('scrollHeight'));
    }, 3000);

    $("#chatTextInput").keypress(function(e){
        var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
        if (charCode) {
            //console.log("Character typed: " + String.fromCharCode(charCode) + " Key #"+charCode);
        }
        if (charCode == 13) { $("#chatSendButton").trigger("click"); }
    });
    $("#chatTextInput").on("click",function(){
        $("#chatTextInput").focus();
    });
        // <p class="name-left"><img class="guest-pic" src="images/animal/chicken.png">กษมพล</p>
        // <div class="bubble left"><p>msg</p></div>
    function censore(string, filters) {
    // "i" is to ignore case and "g" for global "|" for OR match
        var regex = new RegExp(filters.join("|"), "gi");
        return string.replace(regex, function (match) {
            //replace each letter with a star
            var stars = '';
            for (var i = 0; i < match.length; i++) {
                stars += '*';
            }
            return stars;
        });

    }
    function postMsg() {
        var badWordList = $("#badWordList").val();
        //console.log(badWordList);
        // jsonResponse.forEach(function(element) {
        //     console.log(element);
        // });
        //console.log(badWordList);
        var msg     = $("#chatTextInput").val(),
            pic  = $("#guestPic").val(),
            imgSrc  = "images/animal/"+pic,
            name    = $("#guestName").val(),
            idTopic    = $("#idTopic").val(),
            message = document.createElement('div'),
            bubble  = document.createElement('div'),
            msgP    = document.createElement('p'),
            nameP   = document.createElement('p'),
            img     = document.createElement('img');

        var jsonResponse = JSON.parse(badWordList);  //cars will now be the array.
        for(var i = jsonResponse.length-1;i>0;i--){
            msg = censore(msg, jsonResponse[i]);
            //console.log(msg);
        }

        postChat(msg,idTopic,name,pic)
        //console.log(msg);
        if (msg.trim().length <= 0) { return; }
        message.classList.add('message');
        bubble.classList.add('bubble');
        bubble.classList.add('right');
        nameP.classList.add('name-right');
        img.classList.add('guest-pic');
        img.src = imgSrc;
        nameP.textContent = name;
        msgP.textContent = msg;
        nameP.appendChild(img);
        message.appendChild(nameP);
        message.appendChild(bubble);
        bubble.appendChild(msgP);
        $("#chatTextInput").text("");
        $(".message:last").after(message);
        // var chatWindow = $("#chatWindow"),
        //     scrollTo = $(".message:last");
        // chatWindow.scrollTop(
        //     scrollTo.offset().top - chatWindow.offset().top + chatWindow.scrollTop()
        // );
        chatWindow.scrollTop(chatWindow.prop('scrollHeight'));
        $("#chatTextInput").val("");
    }

    $("button").on("click", function(){
        $("div").fadeOut(1000, function(){//fade all div out in 1 second.
            $(this).remove();//remove all div after fade them out.
            //console.log("Fade Completed");
        });
    });//If someone click a button fade all div out in 1 second and then log "Fade Complete" to console.
});