<?php
require_once("define-all-includes.php"); //read including file only when the file hasn't read yet  and if the file is not available, terminate. Stop the page.
try {
    $getSessionQuery = $pdo->query("SELECT `idSession`,`displayName`,`startDate`,`endDate` FROM `session` WHERE `status` = 'open'");
}catch (PDOException $e) {
    die('Query failed: '.$e->getMessage());
}
$getSession = $getSessionQuery->fetch();
$idSession = $getSession['idSession'];
try {
    $getStageQuery = $pdo->query("SELECT `idStage`,`stageName` FROM `stage` WHERE `idSession` = ".$idSession." and `stageStatus` = 'open'");
}catch (PDOException $e) {
    die('Query failed: '.$e->getMessage());
}
$stageCount = 0;
$getStage = array();
$getTopic = array();
while($getStageRow = $getStageQuery->fetch()){
    try {
        $getTopicQuery = $pdo->query("SELECT `idTopic`,`displayName`,`speaker`,`startDate`,`endDate` FROM `topic` WHERE `endDate` >= NOW() and `status` = 'open' and `idStage` = ".$getStageRow['idStage']." ORDER BY `endDate` ASC LIMIT 1");
    }catch (PDOException $e) {
        die('Query failed: '.$e->getMessage());
    }
    $getTopic[$stageCount] = $getTopicQuery->fetch();
    $getStage[$stageCount]['stageName'] = $getStageRow['stageName'];
    $getStage[$stageCount]['idStage'] = $getStageRow['idStage'];
    $stageCount++;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
    <?=$meta?>
</head>
<body class="list-page">
<div class="menu-bar">
    <div class="left"><img class="back-button" src="images/back.png" id="listBackButton"></div>
    <div class="center"><?=$getSession['displayName'];?></div>
    <div class="right"><img class="search-button" src="images/animal/<?=$guestPic?>" id="listSearchButton"></div>
</div>
<div class="container">
    <!-- topic list -->
    <?php
    for($i=0;$i<$stageCount;$i++){
        $idStage = $getStage[$i]['idStage'];
        $stage = $getStage[$i]['stageName'];
        if(!empty($getTopic[$i])){
            $idTopic = $getTopic[$i]['idTopic'];
            $topic = $getTopic[$i]['displayName'];
            $startTime = explode(" ",$getTopic[$i]['startDate']);
            $startTime = explode(":",end($startTime));
            $startTime = $startTime[0].":".$startTime[1];
            $endTime = explode(" ",$getTopic[$i]['endDate']);
            $endTime = explode(":",end($endTime));
            $endTime = $endTime[0].":".$endTime[1];
            $time = $startTime." - ".$endTime;
            $speaker = $getTopic[$i]['speaker'];
        ?>
        <div class="list-of-topic">
            <div class="topic-header"><div><?=$getTopic[$i]['displayName']?></div></div>
            <div class="topic-date-time-speaker">
                <div class="topic-date-time">
                    <div class="tag">ช่วงเวลา</div>
                    <div><?=$time?></div>
                    <div class="tag">สถานที่</div>
                    <div><?=$stage?></div>
                </div>
                <div class="topic-speaker">
                    <div class="tag">ผู้บรรยาย</div>
                    <div><?=$speaker?></div>
                </div>
            </div>
            <div class="topic-ask-button">
                <a href="chat.php?t=<?=base64_encode($idTopic);?>&se=<?=base64_encode($idSession);?>&st=<?=base64_encode($idStage);?>" class="button">ส่งคำถาม</a>
            </div>
        </div>
    <?php }else{?>
        <div class="list-of-topic">
            <div class="topic-header"><div>ขณะนี้ไม่มีสัมมนาในเวทีนี้</div></div>
            <div class="topic-date-time-speaker">
                <div class="topic-date-time">
                    <div class="tag">สถานที่</div>
                    <div><?=$stage?></div>
                </div>
            </div>
        </div>
    <?php }//end if
    }//end for loop?>
    <div class="alert-message" id="alertMessage">สวัสดี! <?=$guestName;?></div>
</div>
<?=$script;?>
<?=$script_list;?>
</body>
</html>