<?
header ("Content-Type: text/html; charset=utf-8");
session_start();
require_once("../connect/pdo-qanda.php");

$message = $_GET['m'];
$idTopic = $_GET['t'];
$guestName = $_GET['n'];
$guestPic = $_GET['p'];
$e="";
try{
    $pdo->beginTransaction();
    //do something
    $pdoPreparePost = $pdo->prepare("INSERT INTO `chatlog` (`message`, `datetime`, `idTopic`, `guestName`, `guestPic`, `status`) VALUES (:message, NOW(), :idTopic, :guestName, :guestPic, 'normal')");
    $pdoPreparePost->execute(array(":message"=>$message,":idTopic"=>$idTopic,":guestName"=>$guestName,":guestPic"=>$guestPic));
    $pdo->commit();
    //if OK commmit it
}catch (Exception $e){
    $pdo->rollback();
    throw $e;
}
if($e){
    echo $e->getMessage();
}else{
    echo "";
}
?>