var xmlHttp;
function getChat(lastChatLogId,idTopic,guestName)
{
xmlHttp=GetXmlHttpObject();
if (xmlHttp==null)
 {
 alert ("Browser does not support HTTP Request");
 return;
 }
var url="ajax/ajax-chat-get.php";
url=url+"?l="+lastChatLogId;
url=url+"&t="+idTopic;
url=url+"&n="+guestName;
url=url+"&sid="+Math.random();
xmlHttp.onreadystatechange=stateChanged;
xmlHttp.open("GET",url,true);
xmlHttp.send(null);
}function stateChanged()
{
if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
 {
    // Do nothing
    var message = JSON.parse(xmlHttp.responseText);  //cars will now be the array.
    message.forEach(function(element) {
        var msg     = element['message'],
            pic  = element['guestPic'],
            imgSrc  = "images/animal/"+pic,
            name    = element['guestName'],
            lastChatLogId = element['idChatLog'],
            message = document.createElement('div'),
            bubble  = document.createElement('div'),
            msgP    = document.createElement('p'),
            nameP   = document.createElement('p'),
            img     = document.createElement('img'),
            newMessageTimeout;
        //console.log(msg);
        if (msg.trim().length <= 0) { return; }
        message.classList.add('message');
        bubble.classList.add('bubble');
        bubble.classList.add('left');
        img.classList.add('guest-pic');
        nameP.classList.add('name-left');
        img.src = imgSrc;
        nameP.textContent = name;
        msgP.textContent = msg;
        nameP.appendChild(img);
        message.appendChild(nameP);
        message.appendChild(bubble);
        bubble.appendChild(msgP);
        //console.log("height: "+chatWindow.height());
        //console.log("scrollTop: "+chatWindow.scrollTop());
        //console.log("propHeight: "+chatWindow.prop('scrollHeight'));
        var chatWindow = $("#chatWindow"),
            scrollTo = $(".message:last");
        if ((chatWindow.prop('scrollHeight') - chatWindow.scrollTop() - 20) <= chatWindow.height()){
            $(".message:last").after(message);
            chatWindow.scrollTop(chatWindow.prop('scrollHeight'));
        }else{
            $(".message:last").after(message);
            $("#newMessage").text("มีข้อความใหม่");
            $("#newMessage").fadeIn(100,function(){
                if(newMessageTimeout){
                    clearTimeout(newMessageTimeout);
                }
                newMessageTimeout = setTimeout(function(){
                    $("#newMessage").fadeOut(100);
                }, 5000);
            });
        }

        $("#lastChatLogId").val(""+lastChatLogId);
    });
 }
}function GetXmlHttpObject()
{
var xmlHttp=null;
try
 {
 // Firefox, Opera 8.0+, Safari
 xmlHttp=new XMLHttpRequest();
 }
catch (e)
 {
 //Internet Explorer
 try
  {
  xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
  }
 catch (e)
  {
  xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
 }
return xmlHttp;
}

var xmlHttp2;
function postChat(message,idTopic,guestName,guestPic)
{
xmlHttp2=GetXmlHttpObject2();
if (xmlHttp2==null)
 {
 alert ("Browser does not support HTTP Request");
 return;
 }
var url="ajax/ajax-chat-post.php";
url=url+"?m="+message;
url=url+"&t="+idTopic;
url=url+"&n="+guestName;
url=url+"&p="+guestPic;
url=url+"&sid="+Math.random();
xmlHttp2.onreadystatechange=stateChanged2;
xmlHttp2.open("GET",url,true);
xmlHttp2.send(null);
}function stateChanged2()
{
if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
 {
    console.log(xmlHttp2.responseText);
 }
}function GetXmlHttpObject2()
{
var xmlHttp2=null;
try
 {
 // Firefox, Opera 8.0+, Safari
 xmlHttp2=new XMLHttpRequest();
 }
catch (e)
 {
 //Internet Explorer
 try
  {
  xmlHttp2=new ActiveXObject("Msxml2.XMLHTTP");
  }
 catch (e)
  {
  xmlHttp2=new ActiveXObject("Microsoft.XMLHTTP");
  }
 }
return xmlHttp2;
}

var xmlHttp3;
function getBadWordList()
{
xmlHttp3=GetXmlHttpObject3();
if (xmlHttp3==null)
 {
 alert ("Browser does not support HTTP Request");
 return;
 }
var url="ajax/ajax-chat-get-bad-word.php";
url=url+"?sid="+Math.random();
xmlHttp3.onreadystatechange=stateChanged3;
xmlHttp3.open("GET",url,true);
xmlHttp3.send(null);
}function stateChanged3()
{
if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
 {
    // Do nothing
    //console.log(xmlHttp3.responseText);

    // var entityMap = {
    //   '&': '&amp;',
    //   '<': '&lt;',
    //   '>': '&gt;',
    //   '"': '&quot;',
    //   "'": '&#39;',
    //   '/': '&#x2F;',
    //   '`': '&#x60;',
    //   '=': '&#x3D;'
    // };
    // function escapeHtml (string) {
    //   return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    //     return entityMap[s];
    //   });
    // }
    // $("#badWordList").val(escapeHtml(jsonResponse));
    var badWordLevel = new Array();
    var highestLevel = 0;
    var jsonResponse = JSON.parse(xmlHttp3.responseText);  //cars will now be the array.
    jsonResponse.forEach(function(element) {
        var badWord     = element['badWord'],
            level  = element['level'],
            language    = element['language'];
        //console.log("badWord: "+badWord,"level: "+level,"language: "+language);
        if(highestLevel < level){
            badWordLevel[level] = new Array();
            highestLevel = level;
        }
        badWordLevel[level].push(badWord);
    });
    // badWordLevel.forEach(function(element){
    //     console.log(element);
    // });
    badWordLevel = JSON.stringify(badWordLevel);
    $("#badWordList").val(badWordLevel);

 }
}function GetXmlHttpObject3()
{
var xmlHttp3=null;
try
 {
 // Firefox, Opera 8.0+, Safari
 xmlHttp3=new XMLHttpRequest();
 }
catch (e)
 {
 //Internet Explorer
 try
  {
  xmlHttp3=new ActiveXObject("Msxml2.XMLHTTP");
  }
 catch (e)
  {
  xmlHttp3=new ActiveXObject("Microsoft.XMLHTTP");
  }
 }
return xmlHttp3;
}