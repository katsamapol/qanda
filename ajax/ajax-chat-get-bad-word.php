<?php
header ("Content-Type: text/html; charset=utf8");
require_once("../connect/pdo-qanda.php");
$e="";
//bad_word_list
//idBadWordList
//badWord
//level
//language
try{
    $pdo->beginTransaction();
    //do something
    $pdoPrepareGet = $pdo->prepare("SELECT `idBadWordList`,`badWord`, `level`, `language` FROM `bad_word_list`");
    $pdoPrepareGet->execute();
    $getBadWordResult = $pdoPrepareGet->fetchAll();
    $pdo->commit();
    //if OK commmit it
}catch (Exception $e){
    $pdo->rollback();
    throw $e;
}
if($e){
    echo $e->getMessage();
}else{
    echo json_encode($getBadWordResult);
}
?>