<?
header ("Content-Type: text/html; charset=utf-8");
session_start();
require_once("../connect/pdo-qanda.php");

$lastChatLogId = $_GET['l'];
$idTopic = $_GET['t'];
$guestName = $_GET['n'];
$getChatResult = array();
$e="";
try{
    $pdo->beginTransaction();
    //do something
    $pdoPrepareGet = $pdo->prepare("SELECT `idChatLog`,`message`, `datetime`, `idTopic`, `guestName`, `guestPic`, `status` FROM `chatlog` WHERE `idTopic` = :idTopic and `idChatLog` > :lastChatLogId and `guestName` != :guestName");
    $pdoPrepareGet->execute(array(":idTopic"=>$idTopic,":lastChatLogId"=>$lastChatLogId,":guestName"=>$guestName));
    $getChatResult = $pdoPrepareGet->fetchAll();
    $pdo->commit();
    //if OK commmit it
}catch (Exception $e){
    $pdo->rollback();
    throw $e;
}
if($e){
    echo $e->getMessage();
}else{
    echo json_encode($getChatResult);
}
// if(sizeof($getChatResult)>0){
//     return json_encode($getChatResult);
// }else{
//     if($e){
//         return $e->getMessage();
//     }
// }

?>