<?php
require_once("define-all-includes.php"); //read including file only when the file hasn't read yet  and if the file is not available, terminate. Stop the page.

/*Google Captcha Code*/
$getCaptchaResponse = false;
$login_error = "";
if(isset($_POST['submit'])){
    $captcha=$_POST['g-recaptcha-response'];

    if(!$captcha)
    {
        $login_error = "<div class=\"alert alert-warning\">กรุณากดปุ่มยืนยัน</div>";
    }

    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfDGTEUAAAAAMjnEHQvZSZdlahtzCDa_orh6c5D&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);

    if($response){
        // print_r($response);
        $response = json_decode($response);
        // print_r($response);
        // echo $response->success;
        // break;
        if($response->success==0){
            $login_error = "<div class=\"alert alert-danger\">คุณยังไม่ได้กดปุ่มยืนยัน</div>";
        }else if($response->success==1){
            //captcha passed check for password
            $getCaptchaResponse = true;
        }
    }
}
if($getCaptchaResponse === true){
    if(isset($_SESSION['guestName']) && isset($_SESSION['guestPic'])){
        header("Location: list.php");
    }else{
        //generate name and go to index
        $generatedVal = "";
        $generatedVal = generateGuestName($pdo);
        if(empty($generatedVal)){
            $login_error = "<div class=\"alert alert-danger\">ระบบเกิดข้อผิดพลาด กรุณาลองอีกครั้ง</div>";
        }else{
            $pieces = explode(",",$generatedVal);
            $_SESSION["guestPic"] = $pieces[0].".png";
            $_SESSION["guestName"] = $pieces[0].$pieces[1];
            //print_r($_SESSION);
        }
        header("Location: list.php");
    }
    $login_error = "<div class=\"alert alert-danger\">ระบบเกิดข้อผิดพลาด กรุณาลองอีกครั้ง</div>";
}
/*End of google captcha code*/


function generateGuestName($pdo){
    //echo "I'm in";
    $randomInput = array();
    $randomNumOfUsed = array();
    $retries = 3;
    $returnValue = "";
    while($retries > 0){
        try {
            $pdo->beginTransaction();
            //echo "Begin Transaction";
            try {
                $pdoNumOfUsed = $pdo->query("SELECT `numberOfUsed` FROM good_name_list limit 1");

            }catch (PDOException $e) {
                die('Query failed: '.$e->getMessage());
            } 
            $numOfUsedChecker = $pdoNumOfUsed->fetch();
            try {
                $pdoPrepare3 = $pdo->prepare("UPDATE `good_name_list` SET numberOfUsed = numberOfUsed + 1 WHERE 1");
                $pdoPrepare3->execute(array());
            }catch (PDOException $e) {
                die('Query failed: '.$e->getMessage());
            }
            try {
                $pdo_query = $pdo->query("SELECT `displayName`,`numberOfUsed` FROM good_name_list");

            }catch (PDOException $e) {
                die('Query failed: '.$e->getMessage());
            }    
            while ($row = $pdo_query->fetch())
            {
                $randomInput[] = $row['displayName'];
                $randomNumOfUsed[] = $row['numberOfUsed'];
            }
            $randedVal= array_rand($randomInput, 1);
            //echo "Randed Value:".$randedVal;
            if(($randomNumOfUsed[$randedVal]-1) == $numOfUsedChecker['numberOfUsed']){
                if($randomNumOfUsed[$randedVal] < 100){
                    $randomNumOfUsed[$randedVal] = "00".$randomNumOfUsed[$randedVal];
                }
                $retries=0;
                $returnValue = $randomInput[$randedVal].",".$randomNumOfUsed[$randedVal];
                $pdo->commit();
            }else{

            }
        }catch (Exception $e){
            $pdo->rollback();
            $retries--;
            //throw $e;
        } 
    }
    return $returnValue;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$title;?></title>
    <?=$meta;?>
    <?=$include;?>
    <?=$include_guest;?>
    <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('captcha', {
          'sitekey' : '6LfDGTEUAAAAAJWvrzaNevQ0E7v-0fEWByMJPno8',
          'theme' : 'dark',
          'size' : 'normal',
          'hl' : 'th'
        });
      };
    </script>
</head>
<?php
// print_r($_SERVER['HTTP_HOST']);
// include("includes/check_cookie.php");
// echo $cookie_name;
//check cookies -> auto tranfers to destination

//validate isRobot

//random thai name picker

//send thai name via ajax to check for available name
?>
<body class="guest-page">
<div class="first-row"></div>
<div class="second-row">
    <div class="first-column">&nbsp;</div>
    <div class="second-column">
    <form name="guestInfomation" id="guestInfomation" method="post" action="<?=$_SERVER['PHP_SELF'];?>">
	<span>ยินดีต้อนรับ<br/>กรุณายืนยันว่าคุณไม่ใช่หุ่นยนต์</span>
	<div id="captcha" class="captcha"></div>
    <?=$login_error;?>
    <!-- Change this to a button or input when using this as a form -->
	<input type="submit" name="submit" class="button" value="Next">
    </form>
    </div>
    <div class="third-column">&nbsp;</div>
</div>
<div class="third-row"></div>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
</script>
<?=$script;?>
<?=$script_guest;?>
</body>
</html>