<?php
require_once("../../connect/pdo-qanda.php");

define('SALT_LENGTH', 20);
function generateHash($plainText, $salt = null)
{
    if ($salt === null)
    {
        $salt = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
    }
    else
    {
        $salt = substr($salt, 0, SALT_LENGTH);
    }
	//echo $salt."<br>";
    return $salt . sha1($salt . $plainText);
}
function checkPassword($user,$pass,$pdo){

    $sql="SELECT `password` FROM `admin` WHERE `username` = :username";
    $searchArray = array(':username'=>$user);
    $pdoPrepare = $pdo->prepare($sql);
    try{
        $pdoPrepare->execute($searchArray);
    }catch(PDOException $e){
        die ('Execute failed: '. $e->getMessage());
    }
    $rs = $pdoPrepare->fetch();
	$salt = substr($rs['password'],0,20);
	$enterPass = $salt . sha1($salt . $pass);
	if($rs['password'] == $enterPass){
		$check = true;
	}else{
		$check = false;
	}
	return $check;
}

function addNewHashedUser($user,$pass,$name,$pdo){
    //Transactions
    $hashedPass = generateHash($pass,null);
    //echo $hashedPass."<br>";
    try{
        $pdo->beginTransaction();
        //do something
        $pdoPrepare = $pdo->prepare("INSERT INTO `admin` (`username`,`password`,`displayName`) VALUES (:username,:password,:displayName)");
        $pdoPrepare->execute(array(":username"=>$user,":password"=>$hashedPass ,":displayName"=>$name));
        $pdo->commit();
        return true;
        //if OK commmit it
    }catch (Exception $e){
        $pdo->rollback();
        throw $e;
    }
}

// if(addNewHashedUser("setstaff2","Setstaff2&","setstaff2",$pdo)){
//     echo "<br/>Success";
// }else{
//     echo "<br/>Failed";
// }

// if(checkPassword("katsamapol","Qbjhrrew2530",$pdo)){
//     echo "check passed";
// }else{
//     echo "check failed";
// }


// $hash = generateHash("Setstaff2&",null);

// echo "$hash<br>"; //insert this one to database

// $salt = substr($hash,0,20);

// echo $salt."<br>"; //remove encript infomation

// $encrypted_text = substr($hash,20);

// echo $encrypted_text;

?>