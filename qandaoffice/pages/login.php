<?php
require_once("../includes/hash.php");
require_once("../includes/session.php");

if(isset($_GET['logout'])){
    setcookie($cookie_username, "", time() - 3600, "/");
}else{
    if(isset($_COOKIE[$cookie_username])){
        header("Location: qandatable.php");
    }
}

$login_error="";
/*Google Captcha Code*/
// if(isset($_POST['submit'])){
//     $username=$_POST['username'];
//     $password=$_POST['password'];
//     $captcha=$_POST['g-recaptcha-response'];

//     if(!$captcha)
//     {
//         $login_error = "<div class=\"alert alert-warning\">Please check the captcha form.</div>";
//     }

//     $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfDGTEUAAAAAMjnEHQvZSZdlahtzCDa_orh6c5D&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);

//     if($response){
//         // print_r($response);
//         $response = json_decode($response);
//         // print_r($response);
//         // echo $response->success;
//         // break;
//         if($response->success==0){
//             $login_error = "<div class=\"alert alert-danger\">Please verify human</div>";
//         }else if($response->success==1){
//             //captcha passed check for password
//             $login_error = "";
//             if(checkPassword($_POST['username'],$_POST['password'],$pdo)){
//                 //echo "check passed<br/>";
//                 if($_POST['remember'] == 1){
//                     $cookie_value = $_POST['username'];
//                     // echo "set cookie.<br/>";
//                     // echo $cookie_username."<br/>";
//                     // echo $cookie_value."<br/>";
//                     // break;
//                     setcookie($cookie_username, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
//                 }
//                 header("Location: index.php");
//                 die();
//             }else{
//                 $login_error = "<div class=\"alert alert-danger\">Username or password is not correct.</div>";
//                 //echo "check failed<br/>";
//             }
//             //echo"<br/>";
//         }
//     }
// }
/*No use Captcha Code*/
if(isset($_POST['submit'])){
    if(checkPassword($_POST['username'],$_POST['password'],$pdo)){
        //echo "check passed<br/>";
        if($_POST['remember'] == 1){
            $cookie_value = $_POST['username'];
            // echo "set cookie.<br/>";
            // echo $cookie_username."<br/>";
            // echo $cookie_value."<br/>";
            // break;
            setcookie($cookie_username, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
        }
        header("Location: index.php");
        die();
    }else{
        $login_error = "<div class=\"alert alert-danger\">Username or password is not correct.</div>";
        //echo "check failed<br/>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- header -->
    <?php include("header.php");?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="<?=$_SERVER['PHP_SELF'];?>" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="1">Remember Me
                                    </label>
                                </div>
                                <!-- <div class="g-recaptcha" data-sitekey="6LfDGTEUAAAAAJWvrzaNevQ0E7v-0fEWByMJPno8" data-size="normal"></div> -->
                                <?=$login_error;?>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" name="submit" class="btn btn-lg btn-success btn-block" value="Login">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- script -->
    <?php include("script.php");?>

</body>

</html>
