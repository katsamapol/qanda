<?php
require_once("define-all-includes.php"); //read including file only when the file hasn't read yet  and if the file is not available, terminate. Stop the page.
$idTopic = base64_decode($_GET['t']);//t
$idSession = base64_decode($_GET['se']);//session
$idStage = base64_decode($_GET['st']);//stage
$lastChatLogId;

try{
    //$getTopicQuery = $pdo->query("SELECT `idTopic`,`displayName` FROM `topic` WHERE `startDate`<=NOW() and `endDate`>=NOW() and `idTopic`=".$idTopic."");
    $getTopicQuery = $pdo->query("SELECT `idTopic`,`displayName` FROM `topic` WHERE `endDate`>=NOW() and `idTopic`=".$idTopic."");
}catch(PDOExeption $e){
    die("Query failed: ".$e.getMessage());
}
$getTopic = $getTopicQuery->fetch();
if(empty($getTopic)){
    header("Location: list.php");
}
$getChatLog = array();
try{
    $getChatLogQuery = $pdo->query("SELECT `idChatLog`,`message`,`datetime`,`idTopic`,`guestName`,`guestPic` FROM `chatlog` WHERE `idTopic`=".$getTopic['idTopic']." and `status` = 'normal'");
}catch(PDOExeption $e){
    die("Query failed: ".$e.getMessage());
}
$getChatLog = $getChatLogQuery->fetchAll();
$lastChatLogId = end($getChatLog)['idChatLog'];

?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
    <?=$meta?>
</head>
<body class="chat-page">
<div class="menu-bar">
    <div class="left"><img class="back-button" src="images/back.png" id="chatBackButton"></div>
    <div class="center"><?=$getTopic['displayName']?></div>
    <div class="right">&nbsp;</div>
</div>
<div class="container" id="chatWindow">
    <div class="message"></div>
    <?php
    foreach($getChatLog as $chatLog){
        if($chatLog['guestName'] == $_SESSION['guestName']){
    ?>
    <div class="message">
        <p class="name-right"><?=$chatLog['guestName'];?><img class="guest-pic" src="images/animal/<?=$chatLog['guestPic']?>"></p>
        <div class="bubble right"><p><?=$chatLog['message']?></p></div>
    </div>
    <?  }else{?>
    <div class="message">
        <p class="name-left"><img class="guest-pic" src="images/animal/<?=$chatLog['guestPic'];?>"><?=$chatLog['guestName'];?></p>
        <div class="bubble left"><p><?=$chatLog['message']?></p></div>
    </div>
    <?  }
    }
    ?>
</div>
<div class="alert-message" id="alertMessage"></div>
<div class="new-message" id="newMessage"></div>
<div class="form" id="inputWindow">
<input id="chatTextInput" type="text" class="text-input" placeholder="ใส่คำถามที่นี่"/>
<input id="chatSendButton" type="button" class="button" value="ส่ง"  />
<input id="guestName" type="hidden" value="<?=$_SESSION['guestName']?>"/>
<input id="guestPic" type="hidden" value="<?=$_SESSION['guestPic']?>"/>
<input id="idTopic" type="hidden" value="<?=$idTopic?>"/>
<input id="lastChatLogId" type="hidden" value="<?=$lastChatLogId?>"/>
<input id="badWordList" type="hidden" value=""/>
</div>
<?php
/*$dir = "images/animal/";
$images_array = glob($dir.'*.png');
foreach($images_array as $data){
    echo substr($data, strpos($data, "/") + 8);
echo $whatIWant."<br>";
}
print_r($images_array);

https://stackoverflow.com/questions/33513957/can-it-flexbox-chat-window-with-input-at-the-bottom-chats-scrolling-up*/
?>
<?=$script;?>
<?=$script_chat?>
</body>
</html>