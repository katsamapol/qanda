<?php
// echo "I'm Control";
session_start();
require_once("connect/pdo-qanda.php");
$guestPic="";
$guestName="";
$currentPage = $_SERVER['PHP_SELF'];
$currentPage = explode("/", $currentPage);
$currentPage = end($currentPage);
if((empty($_SESSION["guestName"]) || empty($_SESSION["guestPic"])) && ($currentPage != "guest.php")){
    header("Location: guest.php");
}else{
    if(isset($_SESSION['guestPic']) && isset($_SESSION['guestName'])){
        $guestPic = $_SESSION['guestPic'];
        $guestName = $_SESSION['guestName'];
    }
}
$title = "SET in the City 2017";
$meta=<<<METADATA
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <link rel="stylesheet" type="text/css" href="styles/standard.css">
    <link rel="stylesheet" type="text/css" href="styles/mobile.css">
    <link rel="stylesheet" media="screen and (min-width:639px)" href="styles/639-above.css">
    <link rel="stylesheet" media="screen and (min-width:1023px)" href="styles/1023-above.css">
METADATA;

$include=<<<INCLUDE
    <?php include("includes/check_cookie.php");?>
INCLUDE;

$include_guest=<<<INCLUDE
INCLUDE;

$script=<<<SCRIPT
    <script src="scripts/jquery-3.2.1.min.js"></script>
    <script src="scripts/jquery-qanda.js"></script>
SCRIPT;

$script_chat=<<<SCRIPT
    <script src="scripts/jquery-qanda-chat.js"></script>
    <script src="ajax/ajax-chat.js"></script>
SCRIPT;

$script_list=<<<SCRIPT
    <script src="scripts/jquery-qanda-list.js"></script>
SCRIPT;

$script_guest=<<<SCRIPT
SCRIPT;
?>