<?php
header ("Content-Type: text/html; charset=utf-8");
//the list of words to check.
// $badWordList = array(
//     'test1',
//     'test2'
//     );

//loop over each bad word
function checkBad($badWordList){
    foreach($badWordList as $k=>$bad){
        //clean up the bad word for use in a regex
        $pattern = '/\b'.preg_quote($bad).'\b/i';

        //replace each bad word
        $msg = preg_replace($pattern, str_repeat('*', strlen($bad)), $msg);
    }
}


?>